# [afwall_easy](https://codeberg.org/angrytux/afwall_easy)

### **Last update: 02.12.2022**

Simple script for generating [ASN](https://en.wikipedia.org/wiki/Autonomous_system_(Internet)) block lists and DNS Server entry for Android AFWall.

Options for dns server (exception for Wifi), captive portal check disabling, ASN blocklists...
**Android version 7 or older and 8+ are supported**

It generates AFWall scripts for blocking connections to google, facebook, whatever you want.
This gets achieved through setting the corressponding iptables rules.

Following **Packages** are **required** to fetch and set the corressponding rules:

**Awk, printf, wget**

_______________________________________________________________________________________________

## *How to use:*

In Terminal:

```git clone https://codeberg.org/angrytux/afwall_easy.git```

```cd afwall_easy```

Make script executable with ```chmod +x afwall.sh```

Run script with ```bash afwall.sh```

_______________________________________________________________________________________________

#### *In Afwall app:*

Custom DNS Server set by this script: Disable DNS via netd- (preferences->Binaries->DNS proxy). 

For DNS over TLS choose enable DNS via netd.

**You must allow (Android 5+) -[0] (root) - Apps running as root- in afwall else dns resolving won´t work!**


_______________________________________________________________________________________________
*Since there isn't an update function yet, the script should be run regularly (e.g monthly) to fetch New ASNs for each company you wish to block. But well this happens not that often.*


***Thx ukanth, Mike and maloe for the amazing work! :+1:***


[ukanth AFWall](https://github.com/ukanth/afwall)  			  

[Mike Kuketz](https://www.kuketz-blog.de/)

[Maloe](https://notabug.org/maloe/ASN_IPFire_Script) 


**License:** All scripts are under [GPLv3](https://www.gnu.org/licenses/gpl.html).



*This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public* *License as published by the Free Software Foundation, either version 3 of the License, or any later version.*

*This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied* *warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.*

*See the GNU General Public License for more details.*

