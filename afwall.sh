#!/bin/bash

## [afwall_easy] Simple script for generating ASN Block lists and DNS Server entry for Android AFWall.

# < https://notabug.org/angrytux/afwall_easy >


# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
# See the GNU General Public License for more details < https://www.gnu.org/licenses >

setdns=1
clear
echo "=================================================="
echo -e "\033[3;34m BLOCK ASNs SCRIPT AND SET DNS FOR ANDROID AFWALL\033[0m"
echo "=================================================="
echo
echo
echo "--------------------------------------------------"
echo "Thx ukanth for the great AFWall app!" 
echo
echo "Visit https://github.com/ukanth/afwall"
echo
echo
echo "Script is based on infos and python scripts from Mike Kuketz,"
echo "https://www.kuketz-blog.de/"
echo
echo "and was further enhanced by maloe"
echo "https://notabug.org/maloe/ASN_IPFire_Script"
echo 
echo
echo "Great job guys!"
echo
echo "--------------------------------------------------"
echo
echo
read -n 1 -s -p "Press any key to continue"
clear
####make ASN fetching script executable####
chmod +x asn_ipfire.sh
####delete and create folder to reset process####
rm -f copy_paste.txt
rm -R -f afwscripts
mkdir afwscripts
####copy common rules####
echo "Which Android version do you use?"
echo
echo "[1] Android 8 and higher"
echo
echo "[2] Android 7 or older"
echo
echo -n ":"
while read version
do
case $version in
1)
cp default.8 ./afwscripts/aafwall_on.sh
cp iptables_off.a8 ./afwscripts/iptables_off.sh
break
;;
2)
cp default ./afwscripts/aafwall_on.sh
cp iptables_off.a7 ./afwscripts/iptables_off.sh
break
;;
esac
done
clear
echo "Set DNS Server? Press key [1-4]"
echo
echo "(You´ll be asked for seperate Wifi dns exception later)"
echo
echo
echo
echo "[1] Don't set custom DNS server (choose this for TLS over DNS)"
echo "[2] 194.150.168.169 AS250 (germany, ad-blocking)"
echo "[3] 91.239.100.100 Censurfridns Denmark"
echo "[4] 80.241.218.68 Dismail (germany)"
echo "[5] 46.182.19.48 Digitalcourage (germany)"
echo "[6] Set own DNS server (ipfire wiki list)"
echo
echo "See https://wiki.ipfire.org/dns/public-servers"
echo "It´s only an overview, not all listed servers are censorship-free!"
echo "At least you should prefer dnssec validating servers."
echo
echo "--------------------------------------------------"
echo -e "\e[3;91mImportant!!!!!"
echo 
echo "If you chose a custom dns server set the DNS proxy to -Disable DNS via netd- (preferences->Binaries->DNS proxy)"
echo "For TLS over DNS set DNS proxy enabled."
echo
echo "You must allow (Android 5+) -[0] (root) - Apps running as root- in afwall else dns resolving won´t work!"
echo "--------------------------------------------------"
echo -e "\e[0m"
echo -n ":"
while read Option
do
case $Option in
1)
setdns=0
break
;;
2)
# AS250.net
dns_ip=194.150.168.169:53
break
;;
3)
# Censurfridns Denmark
dns_ip=91.239.100.100:53
break
;;
4)
# Dismail
dns_ip=80.241.218.68:53
break
;;
5)
# Digitalcourage
dns_ip=46.182.19.48:53
break
;;
6)
echo "Please enter IPv4:"
echo "e.g. 194.150.168.169"
read ip
echo
echo $ip "will be set for dns"
echo
echo
read -n 1 -s -p "Press any key to continue"
dns_ip=$ip
setdns=1
break
esac
done
clear
if [ $setdns == 1 ]; then
echo "Should another DNS server be used for your private Wifi?"
echo "(Set a specific dns server for all networks except home WiFi)"
echo
echo "[1] No, use same dns for mobile and wifi (not set if chosen not to change)"
echo
echo "[2] Yes, use routers dns server(s) for my own Wifi"
echo
echo -n ":"
while read wifi_dns
do
case $wifi_dns in
1)
echo "# DNS Server generated from the script" >> ./afwscripts/aafwall_on.sh
echo "$""IPTABLES -t nat -I OUTPUT -p tcp --dport 53 -j DNAT --to-destination $dns_ip" >>  ./afwscripts/aafwall_on.sh
break
;;
2)
while
    echo "You have to enter your subnet now."
    echo "E.g. If your routers IP is 192.168.178.1 then enter 192.168.178.0/24"
    echo "If it´s 192.168.1.1 enter 192.168.1.0/24"
    echo
    read -p 'subnet:' subnet
    echo "Used subnet: $subnet"
    read -p "Is this correct? [y/n] " yn
    case $yn in
        [Yy]* )  false;;
        * )  true;;
esac
do
    :
done
echo -e "# Set a specific DNS server for all networks except home WiFi\n""$""IPTABLES -t nat -I OUTPUT ! -s $subnet -p tcp --dport 53 -j DNAT --to-destination $dns_ip\n""$""IPTABLES -t nat -I OUTPUT ! -s $subnet -p udp --dport 53 -j DNAT --to-destination $dns_ip" >> ./afwscripts/aafwall_on.sh
break
;;
esac
done
fi
clear
echo "Should the captive portal check be disabled?"
echo
echo "[1] No. Keep captive portal check [default android] !"
echo
echo "[2] Yes. Disable captive portal check!"
echo
echo -n ":"
while read cportal
do
case $cportal in
1)
break
;;
2)
echo -e "\n#Disable captive portal check\npm disable com.android.captiveportallogin \nsettings put global captive_portal_detection_enabled 0\nsettings put global captive_portal_server localhost\nsettings put global captive_portal_mode 0\n" >> ./afwscripts/aafwall_on.sh
if [ $version == 2 ]; then
sed -i '/android.captiveportallogin/d' ./afwscripts/aafwall_on.sh
fi
break
;;
esac
done
clear
path=/data/local
echo "Now we need to set the Path to your internal storage"
echo "Default used: /data/local"
echo
echo "Please check yourself what´s the right path for your device!"
echo
echo "[1] Keep default path /data/local"
echo "[2] Set different path"
echo -n ":"
while read Option2
do
case $Option2 in
1)
echo
echo
path=/data/local
echo "$path/afwscripts will be used"
echo
echo
read -n 1 -s -p "Press any key to continue"
break
;;
2) 
clear
while
    echo "Please enter your path like /data/local"
    echo
    read -p 'New path:' pathinput
    path=$(echo "/"$pathinput | sed 's/^[/]\+/\//; s/[/]\+$//')
    echo "Used path: $path/afwscripts"
    read -p "Is this correct? [y/n] " yn
    case $yn in
        [Yy]* )  false;;
        * )  true;;
esac
do
    :
done
break
esac
done
clear
echo "Now choose the companies you want to be blocked! (Seperate them by comma without space)"
echo "e.g. Google,Facebook,Samsung ..."
echo
echo "Just press enter if you don´t want to block any companies"
echo
echo
read companies
echo
echo $companies "will be blocked"
echo
echo
read -n 1 -s -p "Press any key to continue"
echo
####start fetching ASNs####
./asn_ipfire.sh --afwall "$companies"
mv afwall_rules.txt ./afwscripts/afwall_rules
#### split rules at 100 lines. Else afwall could fail on some devices####
split -l 100 ./afwscripts/afwall_rules ./afwscripts/afwall_rules_
rm ./afwscripts/afwall_rules
ls -d ./afwscripts/* | xargs -n1 basename > ./afwscripts/cp.txt
nl -s ". $path/afwscripts/" ./afwscripts/cp.txt | cut -c7- > copy_paste.txt
rm ./afwscripts/cp.txt
clear
echo "End of script..."
echo
echo "In Afwall app:"
echo "If you dont't want to use DNS over TLS set the DNS proxy to -Disable DNS via netd- (preferences->Binaries->DNS proxy)"
echo "You must allow (Android 5+) -0 - Apps running as root- in afwall else dns resolving won´t work!"
echo
echo "Now copy the whole afwscripts folder to $path on your device."
echo "Additionally transfer copy_paste.txt onto the device."
echo
echo "Device: Open copy_paste.txt and copy all to the clipboard."
echo
echo "Open AFWall and press the three dots in the top right corner and select"
echo "<Set custom script>. Paste the contents of the clipboard to user defined script."
echo 
echo
echo "Set the shutdown script path (user defined shutdown script)."
echo
echo "Enter this:   . $path/afwscripts/iptables_off.sh"
echo
echo -e "\e[3;91mIMPORTANT: Don't forget the ". /" point-space-slash!!"
echo -e "\e[0m"
echo 
echo "Hint: See attached screenshot how it should look like!"
echo
echo "Afterwards press OK and wait until AFWall applied the rules!"
echo
echo
read -n 1 -s -p "Press any key to continue"
clear
echo
echo
echo "The afwall.sh script sets IPv6 to drop!"
echo
echo "Done!"
echo
echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
echo
echo "Have fun and enjoy a bit more privacy!"
echo
echo ""
echo "https://github.com/ukanth/afwall"
echo "https://www.kuketz-blog.de"
echo "https://notabug.org/maloe/ASN_IPFire_Script"
echo
echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
echo
